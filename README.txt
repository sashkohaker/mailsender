If you want to make jar file:
1) fetch project
2) run in console "mvn clean compile assembly:single"

If you want to use this utility:
1) copy in your working directory file "MailSender-1.0-SNAPSHOT-jar-with-dependencies.jar"
2) after that copy in your working diroctory files: "config.properties" and "log.properties"
3) run jar in console "java -jar MailSender-1.0-SNAPSHOT-jar-with-dependencies.jar"

If you want to run jar with logfiles:
1) run in console "java -ea -Djava.util.logging.config.file=log.properties  -jar MailSender-1.0-SNAPSHOT-jar-with-dependencies.jar"

