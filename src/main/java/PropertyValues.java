
import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
/**
 * Created by user on 3/1/2018.
 */
public class PropertyValues {
    //String result = "";
    Properties prop = new Properties();

    Map properties = new HashMap<String, String>();


    public Map getPropValues() throws IOException {

        try {

            FileInputStream confFile = new FileInputStream("config.properties");
            prop.load(confFile);
            //inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
            properties.put("smtpAuth", prop.getProperty("smtpAuth"));
            properties.put("starttlsEnable", prop.getProperty("starttlsEnable"));
            properties.put("smtpHost", prop.getProperty("smtpHost"));
            properties.put("smtpPort", prop.getProperty("smtpPort"));
            properties.put("userEmail", prop.getProperty("userEmail"));
            properties.put("userPassword", prop.getProperty("userPassword"));
            properties.put("subjectEmail", prop.getProperty("subjectEmail"));
            properties.put("sendHtmlEmail", prop.getProperty("sendHtmlEmail"));
            properties.put("fileListOfEmails", prop.getProperty("fileListOfEmails"));
            properties.put("htmlFile", prop.getProperty("htmlFile"));
            properties.put("textMessage", prop.getProperty("textMessage"));
            properties.put("intervalBetweenSending",prop.getProperty("intervalBetweenSending"));
            properties.put("startFromLine",prop.getProperty("startFromLine"));
            properties.put("ccFromList",prop.getProperty("ccFromList"));
            properties.put("toEmail",prop.getProperty("toEmail"));

        } catch (IOException e) {
            System.out.println("property file config.properties not found in the classpath" + e.toString());
        }

        return properties;
    }
    public void setPropValues(String nameProperties, String propValue) throws IOException{
        try {
            FileInputStream confFile = new FileInputStream("config.properties");
            prop.load(confFile);

            prop.setProperty(nameProperties,propValue);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out,"Save startFromLine");
            //prop.list(confFile);
        } catch (IOException e) {
            System.out.println("property file config.properties not found in the classpath" + e.toString());
        }
    }
}
