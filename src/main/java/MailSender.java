import java.io.*;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailSender {
    PropertyValues confProperties = new PropertyValues();

    public MailSender() throws IOException{
        props.put("mail.smtp.auth", confProperties.getPropValues().get("smtpAuth"));
        props.put("mail.smtp.starttls.enable", confProperties.getPropValues().get("starttlsEnable"));
        props.put("mail.smtp.host", confProperties.getPropValues().get("smtpHost"));
        props.put("mail.smtp.port", confProperties.getPropValues().get("smtpPort"));

        session = Session.getInstance(props,
                new javax.mail.Authenticator()  {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication( username, password);
                    }
                });

        Properties preferences = new Properties();
        try {
            FileInputStream configFile = new FileInputStream("log.properties");
            preferences.load(configFile);
            LogManager.getLogManager().readConfiguration();
        } catch (IOException e) {
            System.err.println("Could not setup logger configuration: " + e.toString());
        }
    }

    public static Logger log = Logger.getLogger(MailSender.class.getName());
        String username = String.valueOf(confProperties.getPropValues().get("userEmail"));
        String password = String.valueOf(confProperties.getPropValues().get("userPassword"));

        Properties props = new Properties();
        Session session;

        public String getListOfEmails() throws IOException{
            return String.valueOf(confProperties.getPropValues().get("fileListOfEmails"));
        }
        public String getEmailFrom ()throws IOException{
            return String.valueOf(confProperties.getPropValues().get("userEmail"));
        }
        public int getStartLine() throws IOException{
            return Integer.parseInt(String.valueOf(confProperties.getPropValues().get("startFromLine")));
        }
        public void setStartLine(int sl) throws IOException{
            confProperties.setPropValues("startFromLine", String.valueOf(sl));
        }
        public int getInterval ()throws IOException{
            return Integer.parseInt(String.valueOf(confProperties.getPropValues().get("intervalBetweenSending")));
        }
        public String readFile (String filePath) throws IOException {
            BufferedReader reader = new BufferedReader(new FileReader(String.valueOf(confProperties.getPropValues().get("htmlFile"))));
            String line = null;
            StringBuilder stringBuilder = new StringBuilder();
            //String ls = System.getPropery("line.separator");
            try {
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                return stringBuilder.toString();
            } finally {
                reader.close();
            }
        }

        public void sendEmail(String fromEmail, String toEmail) throws IOException{
        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(fromEmail));
            System.out.println("ccFromList" + confProperties.getPropValues().get("ccFromList"));

            if(String.valueOf(confProperties.getPropValues().get("ccFromList")).equals("true")){
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(String.valueOf(confProperties.getPropValues().get("toEmail"))));
                message.setRecipients(Message.RecipientType.CC,
                        InternetAddress.parse(toEmail));}
                        else if(String.valueOf(confProperties.getPropValues().get("ccFromList")).equals("false")){
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(toEmail));}

            message.setSubject(String.valueOf(confProperties.getPropValues().get("subjectEmail")));

            if (String.valueOf(confProperties.getPropValues().get("sendHtmlEmail")).equals("true"))
            {
                String htmlContent = readFile(String.valueOf(confProperties.getPropValues().get("htmlFile")));
                message.setContent(htmlContent, "text/html; charset=utf-8");
                message.setSentDate(new Date());
            } else {
            message.setText(String.valueOf(confProperties.getPropValues().get("textMessage"))); }

            Transport.send(message);

            log.info("Message was sent to " + toEmail);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }


    public static void main(String[] args) throws IOException, InterruptedException {
       MailSender mailSender = new MailSender();
       //mailSender.setUp();
        String line = null;
            File fl = new File(mailSender.getListOfEmails());
            FileReader frd = new FileReader(fl);
            BufferedReader brd = new BufferedReader(frd);
        int startLine = mailSender.getStartLine();
        int currentLine = 0;


            while ((line=brd.readLine())!=null){

                if (currentLine >= startLine){
                    mailSender.sendEmail(mailSender.getEmailFrom(), line);
                    //System.out.println("Sent to " + line);
                    mailSender.setStartLine(currentLine + 1);
                    //System.out.println("Set startLine " + (currentLine + 1));
                    TimeUnit.SECONDS.sleep(mailSender.getInterval());


                }
                currentLine++;
               /* mailSender.sendEmail(mailSender.getEmailFrom(), line);
                TimeUnit.SECONDS.sleep(mailSender.getInterval());*/
            }

           /* brd.skip()
            System.out.println("Lines in file: " + (countLines.getLineNumber() + 1));*/
            /*for (int i = startLine; i<= countLines.getLineNumber() + 1; i++){


            }*/
                brd.close();
                frd.close();


    }
}